name = "Jessie James"
age = 25
occupation = "Engineer"
movie = "Avengers"
rating = 95.8

print(f"I am {name}, and I am {age} years old, I work as an {occupation}, and my rating for {movie} is {rating}%")


num1 = 5
num2 = 8
num3 = 11

print(num1 * num2)
print(num1 < num3)
print(num2 + num3)
